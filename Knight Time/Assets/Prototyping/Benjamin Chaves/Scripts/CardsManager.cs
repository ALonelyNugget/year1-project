﻿// AUTHOR: Benjamin Chaves

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace CombatPrototype
{
    public class CardsManager : MonoBehaviour
    {
        public GameObject GO_AppManager;
        private AppManager appManager;

        public GameObject GO_CardsDatabase;
        public GameObject GO_HandCardsManager;
        public GameObject GO_TabledCardsManager;

        public HandCardsManager HandCardsManager { get; private set; }
        public CardsDatabase CardsDatabase { get; private set; }
        public TabledCardsManager TabledCardsManager { get; private set; }

        private void Start()
        {
            appManager = GO_AppManager.GetComponent<AppManager>();

            CardsDatabase = GO_CardsDatabase.GetComponent<CardsDatabase>();
            HandCardsManager = GO_HandCardsManager.GetComponent<HandCardsManager>();
            TabledCardsManager = GO_TabledCardsManager.GetComponent<TabledCardsManager>();

            CardsDatabase.PopulateCards();
            HandCardsManager.AllocateCards();

            appManager.UIManager.HandCardsManager.SetBattleCardsInformation();
        }
    }
}