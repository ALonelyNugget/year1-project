﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class GameplayCalculationCard
    {
        public int OffensiveValue { get; private set; }
        public float OffensiveMultiplier { get; private set; }

        public int DefensiveValue { get; private set; }

        public int HealingValue { get; private set; }

        public bool BypassShield { get; private set; }

        public void SetValues(int inputOffensiveValue, float inputOffensiveMultiplier, int inputDefensiveValue, int inputHealingValue, bool inputBypassShield)
        {
            OffensiveValue = inputOffensiveValue;
            OffensiveMultiplier = inputOffensiveMultiplier;

            DefensiveValue = inputDefensiveValue;
            HealingValue = inputHealingValue;

            BypassShield = inputBypassShield;
        }
    }
}