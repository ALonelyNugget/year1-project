﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class SkillCard : Card
    {
        public int OffensiveValue { get; private set; }
        public float OffensiveMultiplier { get; private set; }

        public int DefensiveValue { get; private set; }

        public int HealingValue { get; private set; }

        public bool BypassShield { get; private set; }
        public bool AttackAll { get; private set; }
        public bool InPlayerHand;

        public SkillCard(uint inputCardID, string inputCardName, string inputCardDescription, int inputOffensiveValue, int inputHealingValue, bool inputBypassShield, bool inputAttackAll, float inputOffensiveMultiplier, int inputDefensiveValue)
        {
            CardID = inputCardID;
            CardType = CardTypes.Skill;

            CardName = inputCardName;
            CardDescription = inputCardDescription;

            OffensiveValue = inputOffensiveValue;
            OffensiveMultiplier = inputOffensiveMultiplier;

            DefensiveValue = inputDefensiveValue;

            HealingValue = inputHealingValue;

            BypassShield = inputBypassShield;
            AttackAll = inputAttackAll;
        }
    }
}