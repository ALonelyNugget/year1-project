﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

namespace CombatPrototype
{
    public class UI_TabledCardsManager : MonoBehaviour
    {
        public GameObject GO_AppManager;
        private AppManager appManager;

        public GameObject PlayerTableCard;
        public GameObject EnemyTableCard;

        private void Start()
        {
            appManager = GO_AppManager.GetComponent<AppManager>();

            SetPlayerTableCardBlank();
            SetEnemyTableCardBlank();
        }

        public void SetPlayerTableCardBlank()
        {
            PlayerTableCard.transform.GetChild(1).gameObject.SetActive(false);
            PlayerTableCard.transform.GetChild(2).gameObject.SetActive(false);
        }

        public void SetEnemyTableCardBlank()
        {
            EnemyTableCard.transform.GetChild(1).gameObject.SetActive(false);
            EnemyTableCard.transform.GetChild(2).gameObject.SetActive(false);
        }

        public void UpdatePlayerTableCard()
        {
            Card playerTabledCard = appManager.CardsManager.TabledCardsManager.PlayerTabledCard;

            PlayerTableCard.transform.GetChild(1).gameObject.SetActive(true);
            PlayerTableCard.transform.GetChild(2).gameObject.SetActive(true);

            PlayerTableCard.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = playerTabledCard.CardName;
            PlayerTableCard.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = playerTabledCard.CardDescription;
        }

        public void UpdateEnemyTableCard()
        {
            Card enemyTabledCard = appManager.CardsManager.TabledCardsManager.EnemyTabledCard;

            EnemyTableCard.transform.GetChild(1).gameObject.SetActive(true);
            EnemyTableCard.transform.GetChild(2).gameObject.SetActive(true);

            EnemyTableCard.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = enemyTabledCard.CardName;
            EnemyTableCard.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = enemyTabledCard.CardDescription;
        }
    }
}