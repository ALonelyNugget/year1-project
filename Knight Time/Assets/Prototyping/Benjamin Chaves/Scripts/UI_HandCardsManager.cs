﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using TMPro;

namespace CombatPrototype
{
    public class UI_HandCardsManager : MonoBehaviour
    {
        private const string debugPrefix = "HandCardsManager.cs: ";

        public GameObject GO_AppManager;
        private AppManager appManager;

        public GameObject HandCardsParent;
        public GameObject[] HandCards;

        private void Start()
        {
            appManager = GO_AppManager.GetComponent<AppManager>();
        }

        public void SetHandCardsActive(bool newActive)
        {
            HandCardsParent.SetActive(newActive);
        }

        public void SetBattleCardsInformation()
        {
            for (uint index = 0; index < HandCards.Length; index++)
            {
                Card currentCard = appManager.CardsManager.CardsDatabase.GetCardByPlayerCards(index);

                HandCards[index].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = currentCard.CardName;
                HandCards[index].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = currentCard.CardDescription;
            }
        }

        private int FindCardPlayerCardsID(GameObject cardToFind)
        {
            for (int index = 0; index < HandCards.Length; index++)
            {
                if (HandCards[index] == cardToFind)
                {
                    return index;
                }
            }

            Debug.LogError(debugPrefix + "Failed to find.");
            return -1;
        }

        public void ButtonClickedHandCard(GameObject cardClicked)
        {
            cardClicked.GetComponentInChildren<Button>().interactable = false;
            int hotbarID = FindCardPlayerCardsID(cardClicked); // Acquires which card [as presented to player] was clicked.

            Card cardToPlay = appManager.CardsManager.CardsDatabase.GetCardByPlayerCards((uint)hotbarID);

            CombatCard combatCardToPlay = null;
            SkillCard skillCardToPlay = null;

            if (cardToPlay.CardType == Card.CardTypes.Combat)
            {
                combatCardToPlay = appManager.CardsManager.CardsDatabase.GetCombatCardByDatabaseID(cardToPlay.CardID);
            }
            else
            {
                skillCardToPlay = appManager.CardsManager.CardsDatabase.GetSkillCardByDatabaseID(cardToPlay.CardID);
            }

            appManager.CardsManager.TabledCardsManager.SetPlayerTabledCard(cardToPlay);
            appManager.CardsManager.TabledCardsManager.PlayerTabledCombatCard = combatCardToPlay; // FLAG
            appManager.CardsManager.TabledCardsManager.PlayerTabledSkillCard = skillCardToPlay; // FLAG

            appManager.UIManager.TabledCardsManager.UpdatePlayerTableCard();

            appManager.TurnManager.EndTurn();

            //appManager.TurnManager.PlayCards(combatCardToPlay, skillCardToPlay);
        }
    }
}