﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class TabledCardsManager : MonoBehaviour
    {
        public Card PlayerTabledCard { get; private set; }
        public CombatCard PlayerTabledCombatCard;
        public SkillCard PlayerTabledSkillCard;

        public Card EnemyTabledCard { get; private set; }
        public CombatCard EnemyTabledCombatCard;
        public SkillCard EnemyTabledSkillCard;

        public void SetPlayerTabledCard(Card newPlayerTabledCard)
        {
            PlayerTabledCard = newPlayerTabledCard;
        }

        public void SetEnemyTabledCard(Card newEnemyTabledCard)
        {
            EnemyTabledCard = newEnemyTabledCard;
        }
    }
}