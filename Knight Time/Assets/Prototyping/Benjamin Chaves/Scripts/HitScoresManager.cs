﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CombatPrototype
{
    public class HitScoresManager : MonoBehaviour
    {
        public GameObject HitScoreTemplate;

        private List<RectTransform> hitScores = new List<RectTransform>();

        private void Start()
        {
            HitScoreTemplate.SetActive(false);
        }

        private void Update()
        {
            foreach (RectTransform currentHitScore in hitScores)
            {
                Vector3 currentPosition = currentHitScore.anchoredPosition;
                currentPosition.y += 1.5f * Time.deltaTime;

                currentHitScore.anchoredPosition = currentPosition;
            }
        }

        public void CreateHitScoreOnEnemy(float hitScore)
        {
            GameObject newHitScore = Instantiate(HitScoreTemplate);
            newHitScore.transform.SetParent(HitScoreTemplate.transform.parent);
            newHitScore.SetActive(true);

            newHitScore.GetComponent<TextMeshPro>().text = hitScore.ToString();

            RectTransform newHitScore_RectTrans = newHitScore.GetComponent<RectTransform>();
            newHitScore_RectTrans.anchoredPosition = new Vector3(2f, 0.65f, -0.1f);

            hitScores.Add(newHitScore_RectTrans);
        }

        public void CreateHitScoreOnPlayer(float hitScore)
        {
            GameObject newHitScore = Instantiate(HitScoreTemplate);
            newHitScore.transform.SetParent(HitScoreTemplate.transform.parent);
            newHitScore.SetActive(true);

            newHitScore.GetComponent<TextMeshPro>().text = hitScore.ToString();

            RectTransform newHitScore_RectTrans = newHitScore.GetComponent<RectTransform>();
            newHitScore_RectTrans.anchoredPosition = new Vector3(-2f, 0.65f, -0.1f);

            hitScores.Add(newHitScore_RectTrans);
        }
    }
}