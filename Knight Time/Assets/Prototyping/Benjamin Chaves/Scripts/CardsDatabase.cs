﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class CardsDatabase : MonoBehaviour
    {
        public GameObject GO_HandCardsManager;

        public enum CardTypes
        {
            Empty,          // 0
            Attack,         // 1
            Shield,         // 2
            DoubleAttack,   // 3    
            FastAttack,     // 4
            AttackAll,      // 5
            DragonAttack,   // 6
            DoubleShield,   // 7
            ShieldAttack,   // 8
            Heal1,          // 9
            Heal2,          // 10
            Heal3,          // 11
        }

        private CombatCard[] combatCardsDatabase = new CombatCard[12];
        private SkillCard[] skillCardsDatabase = new SkillCard[12];

        public void PopulateCards()
        {
            combatCardsDatabase[1] = new CombatCard(1, "Attack Card", "Cause 1 Damage", -1, 0); // Attack Card
            combatCardsDatabase[2] = new CombatCard(2, "Shield Card", "Auto count 1 Attack card from others", 0, 1); // Shield Card

            skillCardsDatabase[3] = new SkillCard(3, "Double Attack", "1 character lose 2 Hp", -2, 0, false, false, 0f, 0); // Double Attack
            skillCardsDatabase[4] = new SkillCard(4, "Fast Attack", "1 character lose 1 Hp, ignore any Shield Card", -1, 0, true, false, 0f, 0); // Fast Attack
            skillCardsDatabase[5] = new SkillCard(5, "Attack All", "Each enemy character lose 1 Hp", -1, 0, false, true, 0f, 0); // Attack All
            skillCardsDatabase[6] = new SkillCard(6, "Dragon Attack", "Take half HP", 0, 0, false, false, 0.5f, 0); // Dragon Attack

            skillCardsDatabase[7] = new SkillCard(7, "Double Shield", "Counter 2 Attack", 0, 0, false, false, 0f, 2); // Double Shield
            skillCardsDatabase[8] = new SkillCard(8, "Shield Attack", "Count 1 Attack and cause that character lose 1 Hp", -1, 0, false, false, 0f, 1); // Shield Attack

            skillCardsDatabase[9] = new SkillCard(9, "Heal 1", "1 character get 1 hp, not more than its max Hp", 0, 1, false, false, 0f, 0); // Heal 1
            skillCardsDatabase[10] = new SkillCard(10, "Heal 2", "1 character get 2 hp, not more than its max Hp", 0, 2, false, false, 0f, 0); // Heal 2
            skillCardsDatabase[11] = new SkillCard(11, "Heal 3", "1 character get 3 Hp, not more than its max Hp", 0, 3, false, false, 0f, 0); // Heal 3
        }

        public CombatCard GetCombatCardByDatabaseID(uint cardIndex)
        {
            Debug.Assert(cardIndex != 0);
            Debug.Assert(cardIndex < 3);

            return combatCardsDatabase[cardIndex];
        }

        public SkillCard GetSkillCardByDatabaseID(uint cardIndex)
        {
            return skillCardsDatabase[cardIndex];
        }

        public Card GetCardByDatabaseID(uint cardIndex)
        {
            if (cardIndex < 3)
            {
                return combatCardsDatabase[cardIndex];
            }
            else
            {
                return skillCardsDatabase[cardIndex];
            }
        }

        public Card GetCardByPlayerCards(uint cardIndex)
        {
            int cardDatabaseIndex = GO_HandCardsManager.GetComponent<HandCardsManager>().GetPlayerCardID(cardIndex);

            if (cardDatabaseIndex < 3)
            {
                return combatCardsDatabase[cardDatabaseIndex];
            }
            else
            {
                return skillCardsDatabase[cardDatabaseIndex];
            }
        }
    }
}