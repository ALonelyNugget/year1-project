﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class HealthBarsManager : MonoBehaviour
    {
        public GameObject PlayerObject;
        public GameObject PlayerHealthbarParent;
        public GameObject PlayerHealthbarPlate;
        public GameObject PlayerHealthbar;

        public GameObject EnemyObject;
        public GameObject EnemyHealthbarParent;
        public GameObject EnemyHealthbarPlate;
        public GameObject EnemyHealthbar;

        private const int playerHealthMax = 5;
        public const int EnemyHealthMax = 4;

        public float EnemyCurrentHealth { get; private set; } = EnemyHealthMax;
        public float PlayerCurrentHealth { get; private set; } = playerHealthMax;

        private void Start()
        {
            UpdateHealthBars();
        }

        private void Update()
        {
            PositionOnPlayer();
            PositionOnEnemy();
        }

        private void PositionOnEnemy()
        {
            Vector3 newPosition = EnemyObject.transform.position;
            newPosition.y += 2f;

            EnemyHealthbarParent.transform.position = newPosition;
        }

        private void PositionOnPlayer()
        {
            Vector3 newPosition = PlayerObject.transform.position;
            newPosition.y += 2f;

            PlayerHealthbarParent.transform.position = newPosition;
        }

        private void UpdateHealthBars()
        {
            {
                Vector3 newScale = EnemyHealthbarPlate.transform.localScale;
                newScale.x *= (EnemyCurrentHealth / EnemyHealthMax);

                EnemyHealthbar.transform.localScale = newScale;
            }

            {
                Vector3 newScale = PlayerHealthbarPlate.transform.localScale;
                newScale.x *= (PlayerCurrentHealth / playerHealthMax);

                PlayerHealthbar.transform.localScale = newScale;
            }
        }

        public void ModifyPlayerHealthLiterally(float healthModifier)
        {
            PlayerCurrentHealth += healthModifier;
            PlayerCurrentHealth = Mathf.Clamp(PlayerCurrentHealth, 0f, playerHealthMax);

            UpdateHealthBars();
        }

        public void ModifyEnemyHealthLiterally(float healthModifier)
        {
            EnemyCurrentHealth += healthModifier;
            EnemyCurrentHealth = Mathf.Clamp(EnemyCurrentHealth, 0f, EnemyHealthMax);

            UpdateHealthBars();
        }
    }
}