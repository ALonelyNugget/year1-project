﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class CombatCard : Card
    {
        public int OffensiveValue { get; private set; }
        public int DefensiveValue { get; private set; }

        public CombatCard(uint inputCardID, string inputCardName, string inputCardDescription, int inputOffensiveValue, int inputDefensiveValue)
        {
            CardID = inputCardID;
            CardType = CardTypes.Combat;

            CardName = inputCardName;
            CardDescription = inputCardDescription;

            OffensiveValue = inputOffensiveValue;
            DefensiveValue = inputDefensiveValue;

            CardName = inputCardName;
        }
    }
}