﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class AppManager : MonoBehaviour
    {
        public GameObject GO_CardsManager;
        public GameObject GO_HealthBarsManager;
        public GameObject GO_HitScoresManager;
        public GameObject GO_AIManager;
        public GameObject GO_TurnManager;
        public GameObject GO_UIManager;

        public CardsManager CardsManager { get; private set; }
        public HealthBarsManager HealthBarsManager { get; private set; }
        public HitScoresManager HitScoresManager { get; private set; }
        public AIManager AIManager { get; private set; }
        public TurnManager TurnManager { get; private set; }

        public UIManager UIManager { get; private set; }

        private void Awake()
        {
            CardsManager = GO_CardsManager.GetComponent<CardsManager>();
            HealthBarsManager = GO_HealthBarsManager.GetComponent<HealthBarsManager>();
            HitScoresManager = GO_HitScoresManager.GetComponent<HitScoresManager>();
            AIManager = GO_AIManager.GetComponent<AIManager>();
            TurnManager = GO_TurnManager.GetComponent<TurnManager>();
            UIManager = GO_UIManager.GetComponent<UIManager>();

            AIManager.Initialise();
            UIManager.Initialise();
        }
    }
}