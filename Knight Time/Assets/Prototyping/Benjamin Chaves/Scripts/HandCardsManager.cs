﻿// AUTHOR: Benjamin Chaves

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class HandCardsManager : MonoBehaviour
    {
        private int[] playerHandCards = new int[5];
        private int[] enemyHandCards = new int[5];

        System.Random randomGenerator = new System.Random();

        public void AllocateCards()
        {
            AllocateCombatCards(ref playerHandCards);
            AllocateCombatCards(ref enemyHandCards);

            AllocateSkillCards();
        }

        private void AllocateCombatCards(ref int[] entityHand)
        {
            List<int> cardsList = new List<int>() { 1, 1, 1, 1, 1, 1, 2, 2, 2 };

            for (int cardIndex = 0; cardIndex < 4; cardIndex++)
            {
                int randomNumber = randomGenerator.Next(0, cardsList.Count); // Pick random list index.

                entityHand[cardIndex] = cardsList[randomNumber]; // Assign Card ID at random list index.

                cardsList.RemoveAt(randomNumber); // Remove the picked Card ID from the list.
            }
        }

        private void AllocateSkillCards()
        {
            int randomNumber;

            randomNumber = randomGenerator.Next(3, 12);
            playerHandCards[4] = randomNumber;

            randomNumber = randomGenerator.Next(3, 12);
            enemyHandCards[4] = randomNumber;
        }

        public int GetPlayerCardID(uint handIndex)
        {
            return playerHandCards[handIndex];
        }

        public int GetEnemyDefensivePlay()
        {
            /*
            for(uint handIndex = 0; handIndex < enemyHandCards.Length; handIndex++)
            {

            }
            */

            return 0;
        }
    }
}