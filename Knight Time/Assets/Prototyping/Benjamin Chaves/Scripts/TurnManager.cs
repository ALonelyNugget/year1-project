﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Assertions;

namespace CombatPrototype
{
    public class TurnManager : MonoBehaviour
    {
        private const string debugPrefix = "TurnManager.cs: ";

        public GameObject GO_AppManager;
        private AppManager appManager;

        public GameObject EndTurnButton;

        private bool isPlayerTurn = true;

        private GameplayCalculationCard playerCalculationCard = new GameplayCalculationCard();
        private GameplayCalculationCard enemyCalculationCard = new GameplayCalculationCard();

        private int turnID = 1;

        private void Start()
        {
            appManager = GO_AppManager.GetComponent<AppManager>();
        }

        private void StartTurn()
        {
            ConfigureUI();

            appManager.UIManager.HandCardsManager.SetHandCardsActive(isPlayerTurn);
        }

        private void ConfigureUI()
        {
            //EndTurnButton.SetActive(isPlayerTurn);
        }

        public void EndTurn()
        {
            PlayCards(appManager.CardsManager.TabledCardsManager.PlayerTabledCombatCard, appManager.CardsManager.TabledCardsManager.PlayerTabledSkillCard);

            if (isPlayerTurn)
            {

            }
            else
            {

            }

            isPlayerTurn = !isPlayerTurn; // Toggle the current turn.
            StartTurn();
        }

        public void PlayCards(CombatCard playerCombatCard, SkillCard playerSkillCard)
        {
            Debug.Assert(playerCombatCard != null || playerSkillCard != null);

            if (playerCombatCard != null)
            {
                playerCalculationCard.SetValues(playerCombatCard.OffensiveValue, 0f, playerCombatCard.DefensiveValue, 0, false);
                Debug.Log("TURN " + turnID + ": Player plays " + playerCombatCard.CardName + ".");
            }
            else if (playerSkillCard != null)
            {
                playerCalculationCard.SetValues(playerSkillCard.OffensiveValue, playerSkillCard.OffensiveMultiplier, playerSkillCard.DefensiveValue, playerSkillCard.HealingValue, playerSkillCard.BypassShield);
                Debug.Log("TURN " + turnID + ": Player plays " + playerSkillCard.CardName + ".");
            }

            CombatCard enemyCombatCard = appManager.CardsManager.TabledCardsManager.EnemyTabledCombatCard;
            SkillCard enemySkillCard = appManager.CardsManager.TabledCardsManager.EnemyTabledSkillCard;

            if (enemyCombatCard != null)
            {
                enemyCalculationCard.SetValues(enemyCombatCard.OffensiveValue, 0f, enemyCombatCard.DefensiveValue, 0, false);
                Debug.Log("TURN " + turnID + ": Enemy plays " + enemyCombatCard.CardName + ".");
            }
            else if (enemySkillCard != null)
            {
                enemyCalculationCard.SetValues(enemySkillCard.OffensiveValue, enemySkillCard.OffensiveMultiplier, enemySkillCard.DefensiveValue, enemySkillCard.HealingValue, enemySkillCard.BypassShield);
                Debug.Log("TURN " + turnID + ": Enemy plays " + enemySkillCard.CardName + ".");
            }

            float playerHealthModifierLiteral = 0f;
            float enemyHealthModifierLiteral = 0f;

            // ------------------------------------------------------------------------------------------------

            if (enemyCalculationCard.BypassShield) // Enemy will bypass the player's shield.
            {
                playerHealthModifierLiteral = enemyCalculationCard.OffensiveValue;

                if (enemyCalculationCard.OffensiveMultiplier > 0f)
                {
                    playerHealthModifierLiteral = enemyCalculationCard.OffensiveMultiplier * -appManager.HealthBarsManager.PlayerCurrentHealth;
                }
            }
            else // Enemy will NOT bypass the player's shield.
            {
                playerHealthModifierLiteral = enemyCalculationCard.OffensiveValue + playerCalculationCard.DefensiveValue;

                if (enemyCalculationCard.OffensiveMultiplier > 0f)
                {
                    playerHealthModifierLiteral = enemyCalculationCard.OffensiveMultiplier * -appManager.HealthBarsManager.PlayerCurrentHealth;
                }
            }

            if (playerHealthModifierLiteral > 0)
            {
                playerHealthModifierLiteral = 0;
            }

            playerHealthModifierLiteral += playerCalculationCard.HealingValue;

            // ------------------------------------------------------------------------------------------------

            if (playerCalculationCard.BypassShield) // Player will bypass the enemy's shield.
            {
                enemyHealthModifierLiteral = playerCalculationCard.OffensiveValue;

                if (playerCalculationCard.OffensiveMultiplier > 0f)
                {
                    enemyHealthModifierLiteral = playerCalculationCard.OffensiveMultiplier * -appManager.HealthBarsManager.EnemyCurrentHealth;
                }
            }
            else // Player will NOT bypass the enemy's shield.
            {
                enemyHealthModifierLiteral = playerCalculationCard.OffensiveValue + enemyCalculationCard.DefensiveValue;

                if (playerCalculationCard.OffensiveMultiplier > 0f)
                {
                    enemyHealthModifierLiteral = playerCalculationCard.OffensiveMultiplier * -appManager.HealthBarsManager.EnemyCurrentHealth;
                }
            }

            if (enemyHealthModifierLiteral > 0)
            {
                enemyHealthModifierLiteral = 0;
            }

            enemyHealthModifierLiteral += enemyCalculationCard.HealingValue;

            // ------------------------------------------------------------------------------------------------

            appManager.HealthBarsManager.ModifyPlayerHealthLiterally(playerHealthModifierLiteral);
            appManager.HitScoresManager.CreateHitScoreOnPlayer(playerHealthModifierLiteral);

            appManager.HealthBarsManager.ModifyEnemyHealthLiterally(enemyHealthModifierLiteral);
            appManager.HitScoresManager.CreateHitScoreOnEnemy(enemyHealthModifierLiteral);

            turnID++;
        }
    }
}