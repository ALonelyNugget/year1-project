﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class UIManager : MonoBehaviour
    {
        public GameObject GO_HandCardsManager;
        public UI_HandCardsManager HandCardsManager { get; private set; }

        public GameObject GO_TabledCardsManager;
        public UI_TabledCardsManager TabledCardsManager { get; private set; }

        public void Initialise()
        {
            HandCardsManager = GO_HandCardsManager.GetComponent<UI_HandCardsManager>();
            TabledCardsManager = GO_TabledCardsManager.GetComponent<UI_TabledCardsManager>();
        }
    }
}