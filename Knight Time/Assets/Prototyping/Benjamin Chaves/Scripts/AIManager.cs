﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class AIManager : MonoBehaviour
    {
        public GameObject GO_AppManager;
        private AppManager appManager;

        public void Initialise()
        {
            appManager = GO_AppManager.GetComponent<AppManager>();
        }

        public Card SelectCardToTable()
        {
            float currentHealth = appManager.HealthBarsManager.EnemyCurrentHealth;
            float maxHealth = HealthBarsManager.EnemyHealthMax;

            float healthPercentage = currentHealth / maxHealth;

            if (healthPercentage > 0.6f)
            {

            }
            else if (healthPercentage > 0.3f)
            {

            }
            else // healthPercentage <= 0.3f
            {

            }

            return null;
        }
    }
}