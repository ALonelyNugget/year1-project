﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class Card
    {
        public uint CardID { get; protected set; }
        public enum CardTypes { Undefined, Combat, Skill }
        public CardTypes CardType { get; protected set; }

        public string CardName { get; protected set; }
        public string CardDescription { get; protected set; }
    }
}