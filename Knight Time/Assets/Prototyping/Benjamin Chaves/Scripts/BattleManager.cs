﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CombatPrototype
{
    public class BattleManager : MonoBehaviour
    {
        public int CombatCardsPerTurn;
        public int SkillCardsPerTurn;
    }
}